# blue-brackets - a ghost blog template
This is still a work in progress but it's quickly coming together.

There is a ton of hard coded content because of the above. It's not meant to be used as a template for another ghost blog unless you want to dive into the code of the template.
